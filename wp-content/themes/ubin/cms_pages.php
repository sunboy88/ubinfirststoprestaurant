<?php
/*
* Template Name: Cms pages
*/

?>


<div class="container">
    <section id="signatures">
        <div class=""><h1><span class="before"></span><?php the_title(); ?><span class="after"></span></h1></div>        <div class="clearfix"></div>
    </section>
    <div class="content">
        <?php $content = get_post(get_the_ID())->post_content; ?>
        <?php echo $content; ?>
    </div>
</div>
