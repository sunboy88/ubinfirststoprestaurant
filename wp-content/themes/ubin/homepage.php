<?php
/*
* Template Name: Homepage
*/

?>


<div class="container">
    <section id="signatures">
        <div class=""><h1><span class="before"></span><?php the_title(); ?><span class="after"></span></h1></div>
        <div class="clearfix"></div>
    </section>
</div>
<div class="container">
    <div class="swiper-container" id="home-slider">
        <?php $sliders = get_homepage_sliders(); ?>
        <div class="swiper-wrapper">
            <?php foreach($sliders as $a_slider): ?>
                <div class="swiper-slide">
                    <img src="<?php echo $a_slider['slider_image']; ?>" height="486" width="1366">
                    <div class="slide-text">
                        <?php echo $a_slider['chinese_description']; ?> <br/>
                        <?php echo $a_slider['english_description']; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="prev-btn next-prev-btn"></div>
        <div class="next-btn next-prev-btn"></div>
        <div class="my-pagination"></div>
    </div>
</div>
<div class="container">
    <div class="content">
        <div class="welcome-to-ubin">
            <div class="welcome-content">
                <?php $content = get_post(get_the_ID())->post_content; ?>
               <?php echo $content; ?>
            </div>
            <div class="home-menu-item-block">
                <?php $featured_boxes = get_featured_boxes_homepage(); ?>
                <?php foreach($featured_boxes as $box): ?>
                    <a href="<?php echo $box['url']; ?>">
                        <div class="home-menu-item-box">
                            <img src="<?php echo $box['image']; ?>" height="220" width="324">
                            <div class="image-des"><?php echo $box['title']; ?></div>
                        </div>
                    </a>
                <?php endforeach; ?>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="map-block">
        <div id="googleMap"></div>
    </div>
</div>

<!-- End Document
================================================== -->
