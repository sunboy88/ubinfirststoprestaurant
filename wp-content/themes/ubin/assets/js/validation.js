function validateName(isAlert) {
	// var name = document.contactform.name.value;
	var name = $("input[name='your-name']").val();
	var result = "";
	if (name == "") {
		result += "Name is required \r\n";
	} else {
		var check = /^[a-zA-Z][\sa-zA-Z]*$/.test(name);
		if (check == false) {
			result += "Name is invalid";
		}
	}

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateDateOfBirth(isAlert) {
	// var dayOfBirth = document.contactform.dob.value;
	var dayOfBirth = $("input[name='date-of-birth']").val();
	var result = "";

	if (dayOfBirth != "") {
		var dd = dayOfBirth.split("/")[0];
		var mm = dayOfBirth.split("/")[1];
		var yyyy = dayOfBirth.split("/")[2];

		if (dd < 0)
			result += "Day must be a positive number \r\n";

		if (mm < 0)
			result += "Month must be a positive number \r\n";

		if (yyyy < 0)
			result += "Year must be a positive number \r\n";

		if (/^[0-9]{4}$/.test(yyyy) == false)
			result += "Year is invalid \r\n";
		if (mm < 1 || mm > 12)
			result += "Month is invalid \r\n";
		if ((mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12) && (dd < 1 || dd > 31))
			result += "Day is invalid \r\n";
		if ((mm == "4" || mm == "6" || mm == "9" || mm == "11") && (dd < 1 || dd > 30))
			result += "Day is invalid \r\n";
		if ((mm == "2") && (dd < 1 || dd > 29))
			result += "Day is invalid \r\n";
	}
	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateEmail(isAlert) {
	// var email = document.contactform.email.value;
	var email = $("input[name='your-email']").val();
	var result = "";

	if (email == "") {
		result += "Email is required \r\n";
	} else {
		if (/^.+[@]{1}.+$/.test(email) == false)
			result += "Email is invalid \r\n";
	}
	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateContactNumber(isAlert) {
	// var contactNumber = document.contactform.contact.value;
	var contactNumber = $("input[name='contact-number']").val();
	var result = "";

	if (contactNumber == "")
		result += "Contact number is required \r\n";
	else {
		if (/^[0-9]+$/.test(contactNumber) == false)
			result += "Contact number is invalid \r\n";
	}

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateTable(isAlert) {
	// var table = document.contactform.table.value;
	var table = $("input[name='table']").val();
	var result = "";

	if (table == "")
		result += "Tabble is required \r\n";
	else {
		if (/^[0-9]+$/.test(table) == false)
			result += "table is invalid \r\n";
	}

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateDate(isAlert) {
	// var dd = document.contactform.dateday.value;
	// var mm = document.contactform.datemonth.value;
	// var yyyy = document.contactform.dateyear.value;
	var dd = $("input[name='dateday']").val();
	var mm = $("input[name='datemonth']").val();
	var yyyy = $("input[name='dateyear']").val();

	var result = "";

	if ((mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12) && (dd > 31))
		result += "Date is invalid \r\n";
	if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && (dd > 30))
		result += "Date is invalid \r\n";
	if ((mm == 2) && (dd > 29))
		result += "Date is invalid \r\n";

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validatePerson(isAlert) {
	// var person = document.contactform.person.value;
	var person = $("input[name='person']").val();
	var result = "";

	if (person == "")
		result += "Person is required \r\n";
	else {
		if (/^[0-9]+$/.test(person) == false)
			result += "Person is invalid \r\n";
	}

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function validateRequest(isAlert) {
	// var request = document.contactform.message.value;
	var request = $("textarea[name='your-message']").val();
	var result = "";

	if (request == "")
		result += "Request is required \r\n";

	if (isAlert == true) {
		if (result != "")
			alert(result);
	} else
		return result;
}

function checkAndSubmit() {
	var result = "";
	var checkName = validateName(false);
	var checkDOB = validateDateOfBirth(false);
	var checkEmail = validateEmail(false);
	var checkContactNumber = validateContactNumber(false);
	var checkTable = validateTable(false);
	var checkDate = validateDate(false);
	var checkPerson = validatePerson(false);
	var checkRequest = validateRequest(false);
	result += checkName + "\r\n" + checkDOB + "\r\n" + checkEmail + "\r\n" + checkContactNumber + "\r\n" + checkTable + "\r\n" + checkDate + "\r\n" + checkPerson + "\r\n" + checkRequest + "\r\n";

	if (checkName == "" && checkDOB == "" && checkEmail == "" && checkContactNumber == "" && checkTable == "" && checkDate == "" && checkPerson == "" && checkRequest == "") {
		// document.contactform.submit();
		$("#hidden-submit").click();

	} else
		alert(result);
}