<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

function register_my_scripts(){
    
    wp_register_style('ubin-script-normalize', get_template_directory_uri()  . '/assets/css/normalize.css');
    wp_enqueue_style('ubin-script-normalize');
    wp_register_style('ubin-script-base', get_template_directory_uri()  . '/assets/css/base.css');
    wp_enqueue_style('ubin-script-base');
    wp_register_style('ubin-script-style', get_template_directory_uri()  . '/assets/css/style.css');
    wp_enqueue_style('ubin-script-style');
    wp_register_style('ubin-script-idangerous', get_template_directory_uri()  . '/assets/css/idangerous.swiper.css');
    wp_enqueue_style('ubin-script-idangerous');
    wp_register_style('ubin-script-main', get_template_directory_uri()  . '/assets/css/main.css');
    wp_enqueue_style('ubin-script-main');
    wp_register_style('ubin-script-responsive', get_template_directory_uri()  . '/assets/css/responsive.css');
    wp_enqueue_style('ubin-script-responsive');
    

    wp_register_script('ubin-script-jquery', get_template_directory_uri()  . '/assets/js/jquery.min.js');
    wp_enqueue_script('ubin-script-jquery');
    wp_register_script('ubin-script-idangerous', get_template_directory_uri()  . '/assets/js/idangerous.swiper.min.js');
    wp_enqueue_script('ubin-script-idangerous');
    wp_register_script('ubin-script-fancybox', get_template_directory_uri()  . '/assets/js/jquery.fancybox.js');
    wp_enqueue_script('ubin-script-fancybox');
    wp_register_script('ubin-script-validation', get_template_directory_uri()  . '/assets/js/validation.js');
    wp_enqueue_script('ubin-script-validation');
     wp_register_script('ubin-script-script', get_template_directory_uri()  . '/assets/js/script.js');
    wp_enqueue_script('ubin-script-script');
    wp_register_script('ubin-script-main', get_template_directory_uri()  . '/assets/js/main.js');
    wp_enqueue_script('ubin-script-main');
   
}

add_action('wp_enqueue_scripts', 'register_my_scripts');

function get_homepage_sliders(){
    $result = array();

    $args = array(
        'post_type' => 'homepage_slider',
        'orderby' => 'menu_order',
        'order'   => 'ASC',
    );

    $sliders = get_posts($args);
    if($sliders){
        foreach($sliders as $a_slider){
            $result[$a_slider->ID]['chinese_description'] = get_field('chinese_description',$a_slider);
            $result[$a_slider->ID]['english_description'] = get_field('english_description',$a_slider);
            $result[$a_slider->ID]['slider_image'] = get_field('slider_image',$a_slider);
        }
    }

    return $result;

}

function get_featured_boxes_homepage(){
    $result = array();

    $page = get_post(get_the_ID());
    $featured_boxes = get_field('featured_boxes',$page);
    if($featured_boxes){
        foreach($featured_boxes as $box){
            $img = wp_get_attachment_image_src( get_post_thumbnail_id( $box->ID ), 'single-post-thumbnail');
            $result[$box->ID]['image'] = $img[0];
            $result[$box->ID]['title'] = $box->post_title;
            $result[$box->ID]['url'] = get_permalink($box->ID);
        }
    }

    return $result;
}

function get_food_gallery(){
    $result = array();

    $args = array(
        'posts_per_page' => '-1',
        'post_type' => 'food_gallery',
        'orderby' => 'menu_order',
        'order'   => 'ASC'
    );

    $gallery = get_posts($args);
    if($gallery){
        foreach($gallery as $item){
            $result[$item->ID]['image'] = get_field('food_gallery',$item);
        }
    }

    return $result;

}
