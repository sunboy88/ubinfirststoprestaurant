<?php
/*
* Template Name: Food gallery
*/

?>


<div class="container">
    <section id="signatures">
        <div class=""><h1><span class="before"></span><?php the_title(); ?><span class="after"></span></h1></div>
        <div class="clearfix"></div>
    </section>
    <div class="content">
        <div class="gallery-block">
            <?php $gallery = get_food_gallery(); ?>
            <?php foreach($gallery as $item): ?>
                <a class="fancybox" rel="gallery" href="<?php echo $item['image']; ?>"><img src="<?php echo $item['image']; ?>" alt="" /></a>
            <?php endforeach; ?>
        </div>

    </div>
</div>
