<?php
/*
* Template Name: Menu page
*/

?>
<?php 
$args = array('hide_empty' => false,'parent'=>0);
$terms = get_terms( "menu_type", $args );
?>

<div class="container">
    <section id="signatures">
        <div class=""><h1><span class="before"></span><?php the_title(); ?><span class="after"></span></h1></div>
        <div class="clearfix"></div>
    </section>
    <div class="content">
    <div class="menu-block">
    <?php if($terms!=null):?>
        <?php foreach($terms as $term):
        $child_args = array('hide_empty' => false,'parent'=>$term->term_id);
        $childs = get_terms( "menu_type", $child_args );
        ?>
            <div class="main-menu-title"><?php echo $term->name;?></div>
            <?php if($childs!=null):?>
                <?php foreach($childs as $child):
                $child_fields = get_fields($child);
                ?>
                <?php
                $args = array("post_type" => "menu",
                                'posts_per_page' => -1,
                                // 'orderby'          => 'post_date',
                                // 'order'            => 'ASC',
                                "tax_query" => array(
                                "relation" => "AND"));
                                    
                $args["tax_query"][] =  array(
                                            'taxonomy' => $child->taxonomy,
                                            'field' => 'slug',
                                            'terms' => array( $child->slug),
                                            'include_children' => true,
                                            'operator' => 'IN'
                                          );
                $menus = get_posts($args);
                ?>
                    <div class="main-menu-block">
                        <div class="food-title w60"><?php echo $child->name;?></div>
                        <div class="w40">
                        <?php if($child_fields["size"]=="1"){?>
                            <div class="food-size size">
                                <span>S</span>
                                <span>M</span>
                                <span>L</span>
                            </div>
                        <?php } else if($child_fields["size"]=="0"){?>
                            <p class="w40">SEASONAL PRICE &#24066;&#20215;</p>
                        <?php } else{
                            $sizes = explode(" ", $child_fields["size"]);
                            ?>
                            <div class="food-size">
                            <?php foreach($sizes as $size):?>
                                <span><?php echo $size;?></span>
                            <?php endforeach;?>
                            </div>
                        <?php }?>
                        </div>
                        <div class="main-food-img">
                        <?php if($child_fields["image"]!=null):?>
                            <img src="<?php echo  $child_fields["image"]['url']?>">
                        <?php endif;?>
                        </div>
                        <?php if($menus!=null):?>
                            <?php foreach($menus as $menu):?>
                                <div class="food-row">
                                    <p class="w60"><?php echo $menu->post_title;?></p>
                                    <p class="w40">
                                        <?php if(get_field('seasonal_price',$menu->ID)==true){?>
                                            SEASONAL PRICE &#24066;&#20215;
                                        <?php } else if(get_field('others',$menu->ID)!=''){?>
                                            <?php echo get_field('others',$menu->ID);?>
                                        <?php } else{?>
                                            <div class="food-size">
                                                <span><?php echo get_field('s_price',$menu->ID);?></span>
                                                <span><?php echo get_field('m_price',$menu->ID);?></span>
                                                <span><?php echo get_field('l_price',$menu->ID);?></span>
                                            </div>
                                        <?php }?>
                                    </p>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                        <div class="clearfix"></div>
                        <div class="full-row-food"><?php echo $child->description;?></div>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif;?>
    <div class="text-center menu-text">
        <p class="text-size1">ALL PRICES ARE SUBJECT TO CHANGES WITHOUT PRIOR NOTICE<br/>
            ALL PRICES ARE SUBJECT TO 7 % GST</p> <br/>

        <p>UBIN FIRST STOP RESTAURANT <br/>
            57 LORONG BEKUKONG, CHANGI VILLAGE, SINGAPORE 499173<br/>
            TEL 6546 5905 / 9652 0506</p><br/>

        <p class="bold down">Website : <a href="http://www.ubinfirststoprestaurant.com.sg">www.ubinfirststoprestaurant.com.sg</a></p>

    </div>
    </div>
    <div class="clearfix"></div>
    </div>
</div>
