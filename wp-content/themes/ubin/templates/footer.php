<div class="footer" id="footer">
    <div class="container">
        <div class="w50">
            <div class="logo">
                <a href="#"><img src="<?php echo get_template_directory_uri()  . '/assets/images/logo.jpg' ?>"></a>
            </div>
            <div class="clearfix"></div>
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(
                    array(
                        'theme_location' => 'primary_navigation',
                        'walker' => new Roots_Nav_Walker(),
                        'menu_class' => 'nav nav-footer'
                    )
                );
            endif;
            ?>
        </div>
        <div class="w50 text-center">
            <div class="address">
                <span>In Singapore:</span><br/>
                Ubin First Stop Restaurant ( Changi )<br/>
                57 Lorong Bekukong, Singapore 499173<br/>
                Tel : 6546 5905 Fax : 6546 5910<br/>
                Opens Daily from 11.30am to 11.00 pm
            </div>
            <div class="fb-section">Find us on Facebook: <a href="http://www.facebook.com/pages/Ubin-First-Stop-Restaurant/224251147585280" target="_blank" class="fb-icon"></a></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="copy-right-text">
        Copyright © 2008 ubin first restaurant. All Rights Reserved. Website Hosting by singhost.
    </div>
</div>


