<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 ie ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 ie ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie ie9"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->

<head id="head">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri()  . '/assets/images/favicon.ico' ?>">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri()  . '/assets/images/apple-touch-icon.png' ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri()  . '/assets/images/apple-touch-icon-72x72.png' ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri()  . '/assets/images/apple-touch-icon-114x114.png' ?>">

    <?php wp_head(); ?>
</head>