setTimeout(function(){


/* jquery.scrollTo-1.4.2-min.js
----------------------------------------------- */
/**
 * jQuery.ScrollTo - Easy element scrolling using jQuery.
 * Copyright (c) 2007-2009 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * Date: 5/25/2009
 * @author Ariel Flesler
 * @version 1.4.2
 *
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 */
;(function(d){var k=d.scrollTo=function(a,i,e){d(window).scrollTo(a,i,e)};k.defaults={axis:'xy',duration:parseFloat(d.fn.jquery)>=1.3?0:1};k.window=function(a){return d(window)._scrollable()};d.fn._scrollable=function(){return this.map(function(){var a=this,i=!a.nodeName||d.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!i)return a;var e=(a.contentWindow||a).document||a.ownerDocument||a;return d.browser.safari||e.compatMode=='BackCompat'?e.body:e.documentElement})};d.fn.scrollTo=function(n,j,b){if(typeof j=='object'){b=j;j=0}if(typeof b=='function')b={onAfter:b};if(n=='max')n=9e9;b=d.extend({},k.defaults,b);j=j||b.speed||b.duration;b.queue=b.queue&&b.axis.length>1;if(b.queue)j/=2;b.offset=p(b.offset);b.over=p(b.over);return this._scrollable().each(function(){var q=this,r=d(q),f=n,s,g={},u=r.is('html,body');switch(typeof f){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(f)){f=p(f);break}f=d(f,this);case'object':if(f.is||f.style)s=(f=d(f)).offset()}d.each(b.axis.split(''),function(a,i){var e=i=='x'?'Left':'Top',h=e.toLowerCase(),c='scroll'+e,l=q[c],m=k.max(q,i);if(s){g[c]=s[h]+(u?0:l-r.offset()[h]);if(b.margin){g[c]-=parseInt(f.css('margin'+e))||0;g[c]-=parseInt(f.css('border'+e+'Width'))||0}g[c]+=b.offset[h]||0;if(b.over[h])g[c]+=f[i=='x'?'width':'height']()*b.over[h]}else{var o=f[h];g[c]=o.slice&&o.slice(-1)=='%'?parseFloat(o)/100*m:o}if(/^\d+$/.test(g[c]))g[c]=g[c]<=0?0:Math.min(g[c],m);if(!a&&b.queue){if(l!=g[c])t(b.onAfterFirst);delete g[c]}});t(b.onAfter);function t(a){r.animate(g,j,b.easing,a&&function(){a.call(this,n,b)})}}).end()};k.max=function(a,i){var e=i=='x'?'Width':'Height',h='scroll'+e;if(!d(a).is('html,body'))return a[h]-d(a)[e.toLowerCase()]();var c='client'+e,l=a.ownerDocument.documentElement,m=a.ownerDocument.body;return Math.max(l[h],m[h])-Math.min(l[c],m[c])};function p(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);


/* waypoints.min.js
----------------------------------------------- */
/*
jQuery Waypoints - v1.1.4
Copyright (c) 2011-2012 Caleb Troughton
Dual licensed under the MIT license and GPL license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/MIT-license.txt
https://github.com/imakewebthings/jquery-waypoints/blob/master/GPL-license.txt
*/
(function($,k,m,i,d){var e=$(i),g="waypoint.reached",b=function(o,n){o.element.trigger(g,n);if(o.options.triggerOnce){o.element[k]("destroy")}},h=function(p,o){var n=o.waypoints.length-1;while(n>=0&&o.waypoints[n].element[0]!==p[0]){n-=1}return n},f=[],l=function(n){$.extend(this,{element:$(n),oldScroll:0,waypoints:[],didScroll:false,didResize:false,doScroll:$.proxy(function(){var q=this.element.scrollTop(),p=q>this.oldScroll,s=this,r=$.grep(this.waypoints,function(u,t){return p?(u.offset>s.oldScroll&&u.offset<=q):(u.offset<=s.oldScroll&&u.offset>q)}),o=r.length;if(!this.oldScroll||!q){$[m]("refresh")}this.oldScroll=q;if(!o){return}if(!p){r.reverse()}$.each(r,function(u,t){if(t.options.continuous||u===o-1){b(t,[p?"down":"up"])}})},this)});$(n).scroll($.proxy(function(){if(!this.didScroll){this.didScroll=true;i.setTimeout($.proxy(function(){this.doScroll();this.didScroll=false},this),$[m].selector.scrollThrottle)}},this)).resize($.proxy(function(){if(!this.didResize){this.didResize=true;i.setTimeout($.proxy(function(){$[m]("refresh");this.didResize=false},this),$[m].selector.resizeThrottle)}},this));e.load($.proxy(function(){this.doScroll()},this))},j=function(n){var o=null;$.each(f,function(p,q){if(q.element[0]===n){o=q;return false}});return o},c={init:function(o,n){this.each(function(){var u=$.fn[k].defaults.context,q,t=$(this);if(n&&n.context){u=n.context}if(!$.isWindow(u)){u=t.closest(u)[0]}q=j(u);if(!q){q=new l(u);f.push(q)}var p=h(t,q),s=p<0?$.fn[k].defaults:q.waypoints[p].options,r=$.extend({},s,n);r.offset=r.offset==="bottom-in-view"?function(){var v=$.isWindow(u)?$[m]("viewportHeight"):$(u).height();return v-$(this).outerHeight()}:r.offset;if(p<0){q.waypoints.push({element:t,offset:null,options:r})}else{q.waypoints[p].options=r}if(o){t.bind(g,o)}if(n&&n.handler){t.bind(g,n.handler)}});$[m]("refresh");return this},remove:function(){return this.each(function(o,p){var n=$(p);$.each(f,function(r,s){var q=h(n,s);if(q>=0){s.waypoints.splice(q,1)}})})},destroy:function(){return this.unbind(g)[k]("remove")}},a={refresh:function(){$.each(f,function(r,s){var q=$.isWindow(s.element[0]),n=q?0:s.element.offset().top,p=q?$[m]("viewportHeight"):s.element.height(),o=q?0:s.element.scrollTop();$.each(s.waypoints,function(u,x){if(!x){return}var t=x.options.offset,w=x.offset;if(typeof x.options.offset==="function"){t=x.options.offset.apply(x.element)}else{if(typeof x.options.offset==="string"){var v=parseFloat(x.options.offset);t=x.options.offset.indexOf("%")?Math.ceil(p*(v/100)):v}}x.offset=x.element.offset().top-n+o-t;if(x.options.onlyOnScroll){return}if(w!==null&&s.oldScroll>w&&s.oldScroll<=x.offset){b(x,["up"])}else{if(w!==null&&s.oldScroll<w&&s.oldScroll>=x.offset){b(x,["down"])}else{if(!w&&o>x.offset){b(x,["down"])}}}});s.waypoints.sort(function(u,t){return u.offset-t.offset})})},viewportHeight:function(){return(i.innerHeight?i.innerHeight:e.height())},aggregate:function(){var n=$();$.each(f,function(o,p){$.each(p.waypoints,function(q,r){n=n.add(r.element)})});return n}};$.fn[k]=function(n){if(c[n]){return c[n].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof n==="function"||!n){return c.init.apply(this,arguments)}else{if(typeof n==="object"){return c.init.apply(this,[null,n])}else{$.error("Method "+n+" does not exist on jQuery "+k)}}}};$.fn[k].defaults={continuous:true,offset:0,triggerOnce:false,context:i};$[m]=function(n){if(a[n]){return a[n].apply(this)}else{return a.aggregate()}};$[m].selector={resizeThrottle:200,scrollThrottle:100};e.load(function(){$[m]("refresh")})})(jQuery,"waypoint","waypoints",this);


/* jquery.mousewheel-3.0.6.pack.js
----------------------------------------------- */
/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function(d){function e(a){var b=a||window.event,c=[].slice.call(arguments,1),f=0,e=0,g=0,a=d.event.fix(b);a.type="mousewheel";b.wheelDelta&&(f=b.wheelDelta/120);b.detail&&(f=-b.detail/3);g=f;b.axis!==void 0&&b.axis===b.HORIZONTAL_AXIS&&(g=0,e=-1*f);b.wheelDeltaY!==void 0&&(g=b.wheelDeltaY/120);b.wheelDeltaX!==void 0&&(e=-1*b.wheelDeltaX/120);c.unshift(a,f,e,g);return(d.event.dispatch||d.event.handle).apply(this,c)}var c=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks)for(var h=c.length;h;)d.event.fixHooks[c[--h]]=
d.event.mouseHooks;d.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=c.length;a;)this.addEventListener(c[--a],e,false);else this.onmousewheel=e},teardown:function(){if(this.removeEventListener)for(var a=c.length;a;)this.removeEventListener(c[--a],e,false);else this.onmousewheel=null}};d.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);


/* navbar
----------------------------------------------- */
$(function() {
	// Do our DOM lookups beforehand
	var nav_container = $(".nav-container");
	var nav = $("nav");
	var nav_outlet = $("nav .selector-wrapper");
	
	var top_spacing = 0;
	var waypoint_offset = 0;

	nav_container.waypoint({
		handler: function(event, direction) {			
			if (direction == 'down') {
				nav_container.css({ 'height':nav.outerHeight() });		
				nav.stop().addClass("sticky-nav").css("top",-nav.outerHeight()).animate({"top":top_spacing});
				nav_outlet.removeClass("hide");				
			} else {	
				nav_container.css({ 'height':'auto' });
				nav.stop().removeClass("sticky-nav").css("top",nav.outerHeight()+waypoint_offset).animate({"top":""});
				nav_outlet.addClass("hide");
			}			
		},
		offset: function() {
			return -nav.outerHeight()-waypoint_offset;
		}
	});
	
	var sections = $("section");
	var navigation_links = $("nav a");
	var scroll_links = $("a.scroll[href^=#]");
	
	sections.waypoint({
		handler: function(event, direction) {		
			var active_section;
			active_section = $(this);
			if (direction === "up") active_section = active_section.prev();

			var active_link = $('nav a[href="#' + active_section.attr("id") + '"]');
			//navigation_links.removeClass("selected");
			//active_link.addClass("selected");
		},
		offset:'25%'
	})
	
	// Scrolling to same and different pages
	var jump=function(e)
	{
	   if (e){
		   e.preventDefault();
		   var target = $(this).attr("href");
	   }else{
		   var target = location.hash;
	   }
	   $('html,body').animate(
	   {
		   scrollTop: $(target).offset().top
	   },function()
	   {
		   location.hash = target;
	   });
	}
	$('html, body').hide();
	$(document).ready(function()
	{
		//$('a[href^=#]').bind("click", jump);
		scroll_links.bind("click", jump);
		if (location.hash){
			setTimeout(function(){
				$('html, body').scrollTop(0).show();
				jump();
			}, 0);
		}else{
			$('html, body').show();
		}
	});
	
	/*$('a').click(function(e){
		$('html,body').scrollTo(this.hash, this.hash);
		e.preventDefault();
	});
	
	navigation_links.click( function(event) {
		$.scrollTo(
			$(this).attr("href"),
			{
				duration: 200,
				offset: { 'left':0, 
				//'top':-0.15*$(window).height() offset 15% of viewport�s height
				'top':-90 }
			}
		);
	});*/

});


/* sub menu (outlet)
----------------------------------------------- */
$(function () {
			
	$('#selector li').hover(
	function () {
		$('ul', this).slideDown(100); //show its submenu
		$(this).addClass("active");
		//$('.ar', this).html('&#9650;');
	}, function () {
		$('ul', this).slideUp(100);	 //hide its submenu
		$(this).removeClass("active");
		//$('.ar', this).html('&#9660;');
	});
	
});

/* mobile menu
----------------------------------------------- 
$(function() {
	var pull 		= $('#pull');
		menu 		= $('nav ul');
		menuHeight	= menu.height();
	
	$(pull).click(function(e) {
		e.preventDefault();
		menu.slideToggle();
	});

	$(window).resize(function(){
		var w = $(window).width();
		if(w > 320 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
	});
}); */

/* tinynav.min.js
----------------------------------------------- */
/* http://tinynav.viljamis.com v1.1 by @viljamis */
(function(a,i,g){a.fn.tinyNav=function(j){var b=a.extend({active:"selected",header:"",label:""},j);return this.each(function(){g++;var h=a(this),d="tinynav"+g,f=".l_"+d,e=a("<select/>").attr("id",d).addClass("tinynav "+d);if(h.is("ul,ol")){""!==b.header&&e.append(a("<option/>").text(b.header));var c="";h.addClass("l_"+d).find("a").each(function(){c+='<option value="'+a(this).attr("href")+'">';var b;for(b=0;b<a(this).parents("ul, ol").length-1;b++)c/*+="- "*/;c+=a(this).text()+"</option>"});e.append(c);
b.header||e.find(":eq("+a(f+" li").index(a(f+" li."+b.active))+")").attr("selected",!0);e.change(function(){i.location.href=a(this).val()});a(f).after(e);b.label&&e.before(a("<label/>").attr("for",d).addClass("tinynav_label "+d+"_label").append(b.label))}})}})(jQuery,this,0);

$(function () {
  // TinyNav.js 2
  $('nav #outlet').tinyNav({
	active: 'selected'
  })

});

/* tabs
----------------------------------------------- */
$(document).ready(function() {    
	//When page loads...
	$(".tab-content").hide(); //Hide all content
	$(".tabs-menu li:first").addClass("active").show(); //Activate first tab
	$(".tabs-press li:first").addClass("active").show(); //Activate first tab
	$(".tab-content:first").show(); //Show first tab content

	//On Click Event
	$(".tabs-menu li").click(function() {
		$(".tabs-menu li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab-content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content    can replace fadeIn with show
		return false;
	});

	$(".tabs-press li").click(function() {
		$(".tabs-press li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab-content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content can replace fadeIn with show
		return false;
	});
});


/* fancy box
----------------------------------------------- */
$(document).ready(function() {
	$('.fancybox_gallery').fancybox({
        padding : 5
    });
	$("#fancybox_img").fancybox({
		type : 'image',
		padding : 5,
		openEffect : 'none'
	}).trigger('click');
	$("#fancybox_text").fancybox({
		type : 'inline',
		maxWidth : 700,
		padding : 20,
		openEffect : 'none'
	}).trigger('click');
	$(".iframe").fancybox({
		'width'				: '778',
		'height'			: '428',
        'autoScale'     	: false,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
});


/* facebook
----------------------------------------------- */
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=122556177835444";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(window).bind("load resize", function(){    
  var fb_main_width = $('#facebook-main').width();
    $('#facebook-main').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/pages/Uncle-Leong-Seafood/505972586124025?fref=ts"' +
    'data-width="' + fb_main_width + '" data-height="290"' +
    'data-layout="standard" data-action="like" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>');
    FB.XFBML.parse( );
});

$(window).bind("load resize", function(){    
  var fb_strip_width = $('#facebook-strip').width();
    $('#facebook-strip').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/pages/Uncle-Leong-Seafood/505972586124025?fref=ts"' +
    'data-width="' + fb_strip_width + '" data-height="200"' +
    'data-layout="standard" data-action="like" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>');
    FB.XFBML.parse( );
});

$(window).bind("load resize", function(){    
  var fb_strip_jurong_width = $('#facebook-strip-jurong').width();
    $('#facebook-strip-jurong').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/uncleleongseafoodjurong"' +
    'data-width="' + fb_strip_jurong_width + '" data-height="200"' +
    'data-layout="standard" data-action="like" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>');
    FB.XFBML.parse( );
});

$(window).bind("load resize", function(){    
  var fb_strip_tpy_width = $('#facebook-strip-tpy').width();
    $('#facebook-strip-tpy').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/pages/Uncle-Leong-Seafood-Toa-Payoh-Lor-8/207580779290407"' +
    'data-width="' + fb_strip_tpy_width + '" data-height="200"' +
    'data-layout="standard" data-action="like" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>');
    FB.XFBML.parse( );
});

/*$(window).bind("load resize", function(){    
  var fb_footer_width = $('#facebook-footer').width();
    $('#facebook-footer').html('<div class="fb-like" ' + 
    'data-href="https://www.facebook.com/pages/Uncle-Leong-Seafood/505972586124025?fref=ts"' +
    'data-width="' + fb_footer_width +
    'data-layout="standard" data-action="like" data-colorscheme="dark" data-show-faces="true" data-header="false"></div>');
    FB.XFBML.parse( );
});*/

$(window).bind("load resize", function(){    
  var fb_footer_width = $('#facebook-footer').width();
    $('#facebook-footer').html('<div class="fb-like-box" ' + 
    'data-href="https://www.facebook.com/pages/Uncle-Leong-Seafood/505972586124025?fref=ts"' +
    'data-width="' + fb_footer_width +
    'data-layout="standard" data-action="like" data-colorscheme="dark" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>');
    FB.XFBML.parse( );
});

}, 800);