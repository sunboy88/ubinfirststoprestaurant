$(function() {

});
// window.onload = loadScript;

$(window).load(function() {
	loadScript();
	var mySwiper = $('#home-slider').swiper({
		mode: 'horizontal',
		loop: true,
		calculateHeight: true,
		autoplay: 3000,
		onSlideChangeStart: function() {
			$('.slide-text').hide();
			setTimeout(function() {
				$('.slide-text').fadeIn(400);
			}, 300);
		}
	});
	$(".fancybox").attr('rel', 'gallery').fancybox({
		maxWidth: 1000,
		padding: 5
	});
});

var resize = 0;
$(window).resize(function() {
	resize++;
	setTimeout(function() {
		resize--;
		if (resize === 0) {
			ReCalSlider($('#home-slider'));
		}
	}, 100);
});

function ReCalSlider(obj) {
	var SlideContainer = obj.find('.swiper-wrapper');
	var Slide = obj.find('.swiper-slide');
	var SlideHeight = obj.find('.swiper-slide img').height();
	Slide.height(SlideHeight);
	SlideContainer.height(SlideHeight);
};

function initialize() {
	var contentString = "<div class='MarkContent' style='height: 50px; width: 200px;'>57 Lorong Bekukong, Singapore 499173</div>"
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	var myCenter = new google.maps.LatLng(1.390199, 103.987686);
	var mapProp = {
		center: myCenter,
		zoom: 18,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
	var marker = new google.maps.Marker({
		position: myCenter,
		animation: google.maps.Animation.BOUNCE
	});
	marker.setMap(map);
	infowindow.open(map, marker);
}

function loadScript() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "http://maps.googleapis.com/maps/api/js?key=&sensor=false&callback=initialize";
	document.body.appendChild(script);
}